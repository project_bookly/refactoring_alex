package dhbw.fowler;

import java.util.*;

class Customer {
    private String name;
    private ArrayList<Rental> rentals = new ArrayList<Rental>();
    Customer(String name){
        this.name = name;
    }
    void addRental(Rental arg) {
        rentals.add(arg);
    }
    String getName(){
        return this.name;
    }
    String statement() {
        Iterator<Rental> enumRentals =  rentals.iterator();
        StringBuilder res=new StringBuilder();
        res.append("Rental Record for ");
        res.append(this.getName());res.append("\n");
        res.append("\t");res.append("Title");res.append("\t");
        res.append("\t");res.append("Days");res.append("\t");
        res.append("Amount");res.append("\n");


        while (enumRentals.hasNext()) {
            Rental each =  enumRentals.next();
            //determine amounts for each line
            //show figures for this rental
            res.append("\t");res.append(each.getMovie().getTitle());
            res.append("\t");res.append("\t");
            res.append(each.getDaysRented());res.append("\t");
            res.append(amountFor(each));res.append("\n");
        }
        //add footer lines
        res.append("Amount owed is ");
        res.append(getTotalCharge());res.append("\n");
        res.append("You earned ");
        res.append(getTotalFrequentRenterPoints());
        res.append(" frequent renter points");
        return res.toString();
    }

    private double getTotalCharge() {
        double result = 0;
        for (Rental each : rentals) {
            result += amountFor(each);
        }
        return result;
    }

    private int getTotalFrequentRenterPoints(){
        int result = 0;
        for (Rental each : rentals) {
            result += getFrequentRenterPoints(each);
        }
        return result;
    }

    private int getFrequentRenterPoints(Rental rental){
        if(rental.getMovie().getPriceCode()==2){
            return 2;
        }
        return 1;
    }


    private double amountFor(Rental rental) {
        double thisAmount = 0;

        switch (rental.getMovie().getPriceCode()) {
            case Movie.REGULAR:
                thisAmount += 2;
                if (rental.getDaysRented() > 2)
                    thisAmount += (rental.getDaysRented() - 2) * 1.5;
                break;
            case Movie.NEW_RELEASE:
                thisAmount += rental.getDaysRented() * 3;
                break;
            case Movie.CHILDREN:
                thisAmount += 1.5;
                if (rental.getDaysRented() > 3)
                    thisAmount += (rental.getDaysRented() - 3) * 1.5;
                break;
            default:
                throw new IllegalArgumentException("Incorrect dhbw.fowler.Price Code");
        }

        return thisAmount;
    }

}
    