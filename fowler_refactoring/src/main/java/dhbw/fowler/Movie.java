package dhbw.fowler;


class Movie {
    static final int CHILDREN = 2;
    static final int REGULAR = 0;
    static final int NEW_RELEASE = 1;
    private String title;
    private int price;
    Movie(String newtitle, int priceCode) {
        title = newtitle;
        setPriceCode(priceCode);
    }

    int getPriceCode() {
        return this.price;
    }

    void setPriceCode(int arg) {
        switch (arg) {
            case REGULAR:
                price = REGULAR;
                break;
            case CHILDREN:
                price =CHILDREN;
                break;
            case NEW_RELEASE:
                price = NEW_RELEASE;
                break;
            default:
                throw new IllegalArgumentException("Incorrect dhbw.fowler.Price Code");
        }
    }
    String getTitle(){
        return title;
    }

}