package dhbw.fowler;


import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;


public class MovieTest {


    @Test
    public void getPriceCode() {
        Movie DeadPool3=new Movie("Dead Pool 3",1);
        assertThat(DeadPool3.getPriceCode(),is(1));
    }

    @Test
    public void setPriceCode() {
        Movie DeadPool3=new Movie("Dead Pool 3",0);
        DeadPool3.setPriceCode(1);
        assertThat(DeadPool3.getPriceCode(),is(1));
    }

    @Test
    public void getTitle() {
       Movie DeadPool3=new Movie("Dead Pool 3",1);
       assertThat(DeadPool3.getTitle(),is("Dead Pool 3"));
    }

}
