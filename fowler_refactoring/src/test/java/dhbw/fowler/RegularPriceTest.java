package dhbw.fowler;


import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class RegularPriceTest {

    @Test
    public void getPriceCode() {
        Movie matrix=new Movie("matrix",0);
        assertThat(matrix.getPriceCode(),is(0));
        Movie mulan=new Movie("mulan",2);
        assertThat(mulan.getPriceCode(),is(2));
    }


}
