package dhbw.fowler;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class RentalTest {

    @Test
    public void getMovie() {
        Movie Mulan=new Movie("Mulan",2);
        assertThat(Mulan.getTitle(),is("Mulan"));
    }

}
